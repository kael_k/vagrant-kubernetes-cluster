# Vagrant Kubernetes Cluster

Improved version of this vagrant file:
https://gist.github.com/danielepolencic/ef4ddb763fd9a18bf2f1eaaa2e337544

This version use `generic/ubuntu2004`, which is compatible with all principal providers.
The `Vagrantfile` contains alread a default config for settings for `libvirt` and `virtualbox`.

You can tweak the memory, cpu and the list of workers directly from the config part of the Vagrantfile. 

All you need is the nfs to share the project folder across the machines (it will be used by the master to share the join command to the workers).

## FAQ
### For some reason the config of the worker broke during the provision
* check if there is `join.sh` with the `kubeadm join` command
    * true: manually run the provision of the node `vagrant provision <my_unprovisioned_worker>`
    * false: try run first the provision on the master and then on the single workers, or try to re-run directly `vagrant provision`
### Where i can find the kubeconfig
  * the original kubeconfig is in the master node in /etc/kubernetes/admin.conf, it could be found also in vagrant@master home directory, under ~/.kube/config.
  * if you are lazy or in a hurry, remember that you can also use kubectl directly from vagrant@master