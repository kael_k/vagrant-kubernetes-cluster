#!/bin/bash

while [ ! -f /vagrant/join.sh ] || [ "$(wc -l < /vagrant/join.sh)" -ne "2" ]; do
  echo "Cluster join script not detected (/vagrant/join.sh), waiting for script, retrying in 5s...";
  sleep 5
done;

sudo /vagrant/join.sh
echo 'Environment="KUBELET_EXTRA_ARGS=--node-ip=10.0.0.#{ARGV[0]}"' | sudo tee -a /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
sudo systemctl daemon-reload
sudo systemctl restart kubelet