#!/bin/bash

SYNC_HOST_MARKER="vagrant-sync_host-provisioned"
TARGET=/etc/hosts
N_WORKERS=$1

echo "Provisioning /etc/hosts for \"$(hostname)\", inserting master and worker{1..$N_WORKERS}"

function add_entry(){
    echo "$1 $2 #$SYNC_HOST_MARKER" >> $TARGET
}

sed -i "/$SYNC_HOST_MARKER/d" "$TARGET"

add_entry 10.0.0.10 master
for worker_id in $(seq 1 $N_WORKERS);do
    add_entry 10.0.0.$((99+worker_id)) worker$worker_id
done
