################################
### Cluster configuration
################################

$MASTER_NODE_CPU = 2
$MASTER_NODE_MEMORY = 4096

$N_WORKERS = 4
$WORKER_NODES_CPU = 1
$WORKER_NODES_MEMORY = 4096

################################

$WORKER_LIST = []
(1..$N_WORKERS).each do |workder_id|
  $WORKER_LIST.append("worker" + "#{workder_id}")
end


Vagrant.configure("2") do |config|

  # master node config
  config.vm.define :master do |master|
    master.vm.provider :libvirt do |libvirt|
        libvirt.cpus = $MASTER_NODE_CPU
        libvirt.memory = $MASTER_NODE_MEMORY
    end

    master.vm.provider :virtualbox do |v|
      v.cpus = $MASTER_NODE_CPU
      v.memory = $MASTER_NODE_MEMORY
    end
    master.vm.box = "generic/ubuntu2004"
    master.vm.hostname = "master"
    master.vm.network :private_network, ip: "10.0.0.10"

    master.vm.provision :shell, privileged: false, path: 'provision/scripts/setup_master_node.sh'
  end

  # worker node configs
  $WORKER_LIST.each_with_index do |name, i|
    config.vm.define name do |worker|
      
      worker.vm.provider :libvirt do |libvirt|
          libvirt.cpus = $WORKER_NODES_CPU
          libvirt.memory = $WORKER_NODES_MEMORY
      end

      worker.vm.provider :virtualbox do |v|
        v.cpus = $WORKER_NODES_CPU
        v.memory = $WORKER_NODES_MEMORY
      end

      worker.vm.box = "generic/ubuntu2004"
      worker.vm.hostname = name
      worker.vm.network :private_network, ip: "10.0.0.#{i + 100}"

      worker.vm.provision :shell, privileged: false, path: 'provision/scripts/setup_worker_node.sh',  :args => ["#{i + 100}"]
    end
  end


  # /etc/hosts provision
  config.vm.provision :shell, :run => 'always', privileged: true, path: 'provision/scripts/sync_hosts.sh', :args => ["#{$N_WORKERS}"]
  
  # basic initial setup for both master and workers
  config.vm.provision :shell, privileged: true, path: 'provision/scripts/basic_install.sh'

  # Required to share /vagrant/join.sh from master to workers
  config.vm.synced_folder './shared', '/vagrant', type: 'nfs', nfs_version: 4
  
  end